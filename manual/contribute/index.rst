.. _about-user-contribute:

############################
  Contribute Documentation
############################

The Blender Manual is a community driven effort to which anyone can contribute.
Whether you like to fix a tiny spelling mistake or rewrite an entire chapter,
your help with the Blender manual is most welcome!

If you find an error in the documentation, please `report the problem
<https://projects.blender.org/blender/documentation/issues/new>`__.

Get involved in discussions through any of the project `Contacts`_.


.. _about-getting-started:

Getting Started
===============

The following guides lead you through the process.

.. toctree::
   :maxdepth: 1

   install/index.rst
   build.rst
   editing.rst
   pull_requests.rst


Where to help
=============

.. toctree::
   :maxdepth: 1

   todo_list.rst


Guidelines
==========

.. toctree::
   :maxdepth: 1

   guides/writing_guide.rst
   guides/markup_guide.rst
   guides/commit_guide.rst
   guides/templates.rst
   guides/maintenance_guide.rst
   release_cycle.rst


Translations
============

.. toctree::
   :maxdepth: 1

   translations/contribute.rst
   translations/style_guide.rst


.. _contribute-contact:

Contacts
========

`Project Page <https://projects.blender.org/blender/documentation>`__
   An overview of the documentation project.
`Documentation Forum <https://devtalk.blender.org/c/documentation/12>`__
   A forum based discussions on writing and translating documentation.
   This includes the user manual, Wiki, release notes, and code docs.
:ref:`blender-chat`
   ``#docs`` channel for informal discussions in real-time.
`Project Workboard <https://projects.blender.org/blender/documentation/projects>`__
   Manage tasks such as bugs, todo lists, and future plans.
